<?php

require_once('vendor/autoload.php');

use FatturaElettronicaPhp\FatturaElettronica\DigitalDocument;

$file = dirname(__FILE__) . '/tests/fixtures/IT01234567890_FPR02.xml';
//var_dump($file);
$xml = simplexml_load_file($file);
//var_dump($xml);
$eDocument = DigitalDocument::parseFrom($xml);
var_dump(dirname(__FILE__) . '/from_xml.txt');
$eDocument->write(dirname(__FILE__) . '/from_xml.xml');
(DigitalDocument::parseFrom($eDocument->serialize()))->write(dirname(__FILE__) . '/from_dd.xml');