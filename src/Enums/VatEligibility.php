<?php

namespace FatturaElettronicaPhp\FatturaElettronica\Enums;

/**
 * @method static self I()
 * @method static self D()
 * @method static self S()
 */
class VatEligibility extends \Spatie\Enum\Enum
{
    const MAP_VALUE = [
        'I' => 'IVA ad esigibilità immediata',
        'D' => 'IVA ad esigibilità differita',
        'S' => 'Scissione dei Pagamenti',
    ];
}