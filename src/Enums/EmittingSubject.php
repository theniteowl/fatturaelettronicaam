<?php

namespace FatturaElettronicaPhp\FatturaElettronica\Enums;

/**
 * @method static self CC()
 * @method static self TZ()
 */
class EmittingSubject extends \Spatie\Enum\Enum
{
    const MAP_VALUE = [
        'CC' => 'Cessionario / Committente',
        'TZ' => 'Terzo'
    ];
}