<?php

namespace FatturaElettronicaPhp\FatturaElettronica\Enums;

/**
 * @method static self FPA12()
 * @method static self FPR12()
 */
class TransmissionFormat extends \Spatie\Enum\Enum
{
    const MAP_VALUE = [
        'FPA12' => 'Pubblica Amministrazione (P.A.)',
        'FPR12' => 'Privati (B2B / B2C)'
    ];
}