<?php

namespace FatturaElettronicaPhp\FatturaElettronica\Enums;

/**
 * @method static self SC()
 * @method static self MG()
 */
class DiscountType extends \Spatie\Enum\Enum
{
    const MAP_VALUE = [
        'SC' => 'Sconto',
        'MG' => 'Maggiorazione',
    ];
}