<?php

namespace FatturaElettronicaPhp\FatturaElettronica\Enums;

/**
 * @method static self LS()
 * @method static self LN()
 */
class WoundUpType extends \Spatie\Enum\Enum
{
    const MAP_VALUE = [
        'LN' => 'Non in Liquidazione',
        'LS' => 'In Liquidazione'
    ];
}
