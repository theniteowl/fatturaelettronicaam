<?php

namespace FatturaElettronicaPhp\FatturaElettronica\Enums;

/**
 * @method static self TP01()
 * @method static self TP02()
 * @method static self TP03()
 */
class PaymentTerm extends \Spatie\Enum\Enum
{
    const MAP_VALUE = [
        'TP01' => 'Pagamento a Rate',
        'TP02' => 'Pagamento Completo',
        'TP03' => 'Anticipo',
    ];
}