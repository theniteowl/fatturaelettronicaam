<?php

namespace FatturaElettronicaPhp\FatturaElettronica\Enums;

/**
 * @method static self TD01()
 * @method static self TD02()
 * @method static self TD03()
 * @method static self TD04()
 * @method static self TD05()
 * @method static self TD06()
 */
class DocumentType extends \Spatie\Enum\Enum
{
    const MAP_VALUE= [
        'TD01' => 'Fattura',
        'TD02' => 'Acconto / Anticipo su Fattura',
        'TD03' => 'Acconto / Anticipo su Parcella',
        'TD04' => 'Nota di Credito',
        'TD05' => 'Nota di Debito',
        'TD06' => 'Parcella',
    ];
}