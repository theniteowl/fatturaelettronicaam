<?php

namespace FatturaElettronicaPhp\FatturaElettronica\Enums;

/**
 * @method static self RT01()
 * @method static self RT02()
 */
class DeductionType extends \Spatie\Enum\Enum
{
    const MAP_VALUE = [
        'RT01' => 'Ritenuta Persone Fisiche',
        'RT02' => 'Ritenuta Persone Giuridiche',
    ];
}