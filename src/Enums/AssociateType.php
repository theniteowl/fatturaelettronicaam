<?php

namespace FatturaElettronicaPhp\FatturaElettronica\Enums;

/**
 * @method static self SU()
 * @method static self SM()
 *
 */
class AssociateType extends \Spatie\Enum\Enum
{
    const MAP_VALUE = [
            'SU' => 'Socio Unico',
            'SM' => 'Più Soci',
    ];
}