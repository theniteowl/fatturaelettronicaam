<?php

namespace FatturaElettronicaPhp\FatturaElettronica\Enums;

/**
 * @method static self SC()
 * @method static self PR()
 * @method static self AB()
 * @method static self AC()
 *
 */
class CancelType extends \Spatie\Enum\Enum
{
    const MAP_VALUE = [
        'SC' => 'Sconto',
        'PR' => 'Premio',
        'AB' => 'Abbuono',
        'AC' => 'Spesa Accessoria'
    ];
}